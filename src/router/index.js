import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from "@/views/Login.vue"
import Admin from "@/views/Admin.vue"
import EditPage from "../views/EditPage.vue"
import PostPage from '../views/PostPage.vue'
import Test from "@/views/test.vue"

Vue.use(VueRouter)

const routes = [
 	{
 		path:"/test",
 		component:Test
 	},
    {
		path: '/',
		name: 'Home',
		component: Home
	}, {
		path: "/login",
		name: "login",
		component: Login
	}, {
		path: "/admin",
		name: "admin",
		component: Admin,
		meta: "首页",
		children: [{
				path: 'postpage',
				component: PostPage,
				meta: "文章列表"
			},
			{
				path: 'editpage',
				component: EditPage,
				meta: "发表文章"
			}

		]
	}

]

const router = new VueRouter({
	routes
})

export default router