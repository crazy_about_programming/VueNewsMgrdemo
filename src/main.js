import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from "axios";
//设置路由守卫
router.beforeEach((to, from, next) => {
	//	如果不是登录页,先验证有没有token
	if(to.path != "/login") {
		if(!localStorage.getItem("token")) {
			//回到登录页
			return router.push('/login').catch(err => {
        //这里catch捕获了错误，在前端就不会显示了
      });
		}
	}
	//其他页面满足要求则放行
	next();

})

//引入UI库
import ElementUI from 'element-ui';
//引入css
import 'element-ui/lib/theme-chalk/index.css';
//注册组件库
Vue.use(ElementUI);
//绑定到原型
Vue.prototype.$axios = axios;
//设置默认的api域名
axios.defaults.baseURL = "http://liangwei.tech:3000";
//设置请求拦截器,自动带上token
axios.interceptors.request.use((config)=>{
	if(!config.headers.Authorization&&localStorage.getItem("token")){
		//如果拦截器的配置当中的headers头部当中没有token的授权，且本地又有token,那么将自动带上token
		config.headers.Authorization=localStorage.getItem("token");
	}
//	放行请求配置
return config;
})
Vue.config.productionTip = false

new Vue({
	router,
	render: function(h) {
		return h(App)
	}
}).$mount('#app')